import { h , Component } from 'preact';
import style from './style';

class Features extends Component {
    state = {  }
    render = props => { 
        return ( 
            <div class={`${style.hrfeatures}`}>
                <div class={`${style.container}`}>
                    {
                        (props.text && props.text.length > 0) ? (
                            <div class={`${style.title}`}>
                                {
                                    props.text.map((txt,i)=>{
                                        return(
                                                <p>{txt}</p>
                                            )
                                    })
                                }
                            </div>
                        ):null
                    }
                    {
                        (props.icons && props.icons.length > 0) ? (
                            <div class={`${style.features}`}>
                                {
                                    props.icons.map((icn,i)=>{
                                        return(
                                            <div class={`${style.flexbox}`}>
                                                <img src={icn.src}/>
                                                <h2>{icn.title}</h2>
                                                <p>{icn.text}</p>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        ):null
                    }
                </div>
            </div>
         );
    }
}
 
export default Features;