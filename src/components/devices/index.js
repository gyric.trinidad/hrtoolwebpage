import { h , Component } from 'preact';
import style from "./style";

class Devices extends Component {
    state = {  

    }

    render = props => { 
        return ( 
            <div class={`${style.hrdevices}`}>
                <div class={`${style.container}`}>
                    {
                        (props.text && props.text.length > 0) ? (
                            <div class={`${style.title}`}>
                                {
                                    props.text.map((txt,i)=>{
                                        return(
                                            <p>{txt}</p>
                                        )
                                    })
                                }
                            </div>
                        ):null
                    }
                    {
                        (props.images && props.images.length > 0) ? (
                            <div class={`${style.body}`}>
                                {
                                    props.images.map((img,i)=>{
                                        return(
                                            <img key={i} src={img.src}></img>
                                        )
                                    })
                                }
                            </div>
                        ):null
                    }
                    <div class={`${style.footer}`}>
                        {
                            (props.images && props.images.length > 0) ? (
                                <div class={`${style.labelcon}`}>
                                    {
                                        props.images.map((img,i)=>{
                                            return(
                                                <p>{img.text}</p>
                                            )
                                        })
                                    }
                                </div>
                            ):null
                        }
                    </div>
                </div>
            </div>
         );
    }
}
 
export default Devices;