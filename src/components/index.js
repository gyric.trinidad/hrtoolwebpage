export { default as Form} from './form'
export { default as Devices} from './devices'
export { default as Price} from './price'
export { default as Features} from './features'