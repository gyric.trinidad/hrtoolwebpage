import { h , Component } from 'preact';
import style from './style';

class Form extends Component {

    state = {
        
    }
   
    render = props => {
        return (
           <div class={`${style.hrform}`}>
              <div class={`${style.container}`}>
                  { 
                    (props.text && props.text.length > 0) ? (
                      <div class={`${style.title}`}>
                        {
                          props.text.map((txt,i)=>{
                            return(
                              <p>{txt}</p>
                            )
                          })
                        }
                      </div>
                    ):null
                  }
                  <div class={`${style.form}`}>
                    <div class={`${style.textbox}`}>
                      <input type="text" name="name" placeholder="Name"/>
                    </div>
                    <div class={`${style.textbox}`}>
                      <input type="text" name="email" placeholder="Email"/>
                    </div>
                    <div class={`${style.textbox}`}>
                      <input type="text" name="contactno" placeholder="Contact No."/>
                    </div>
                    <div class={`${style.textbox}`}>
                     <textarea name="message" cols="100" rows="8" placeholder="Message"/>
                    </div>
                    <div class={`${style.button}`}>
                      {
                        props.button ? (
                          props.button.type == "text" ? 
                          (<a href={props.button.link.url} target={props.button.link.openInNewTab ? "_blank" : "_self"}>
                            {props.button.text}
                          </a>
                          ):null
                        ):null
                      }
                    </div>
                  </div>
              </div>
           </div>
        )
      };
}

export default Form;