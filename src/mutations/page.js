
import { updateStore, store } from '../unistore';
import xhrWorker from '../workers/xhrWorker';

let xhrWorkerInstace = null;
export function fetchPageData (page) {
  if (xhrWorkerInstace) {
    xhrWorkerInstace.terminate();

    // eslint-disable-next-line
    console.log(`SPA >> Terminated fetchPageData worker. ${page}`);
  }

  // curreny state
  const globalState = store.getState();

  // initial state
  store.setState({
    [page]: {
      ...globalState[page],
      fetching: true,
      result: false
    }
  });
  

  return new Promise(resolve => {
    xhrWorkerInstace = new xhrWorker();

    // worker response
    xhrWorkerInstace.onmessage = ev => {
      if ('result' in ev.data) {
        store.setState({
          [page]: {
            data: ev.data.result,
            fetching: false,
            result: true
          }
        });
      } else {
        store.setState({
          [page]: {
            ...globalState[page],
            fetching: false,
            result: false
          }
        });
      }
      // reset worker
      xhrWorkerInstace.terminate();
      xhrWorkerInstace = null;

      // eslint-disable-next-line
      console.log('SPA >> fetchPageData : [completed] worker terminated.');

      resolve();
    };

    // worker call
    xhrWorkerInstace.postMessage({
      //page = filename
      url: `/assets/data/${page}.json`,
    });
  });
}
