import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { Provider } from 'unistore/preact';
import { store } from '_unistore';

// Code-splitting is automated for routes
import Grid from '_routes/grid';

export default class App extends Component {
	
	/** Gets fired when the route changes.
	 *	@param {Object} event		'change' event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div id='app'>
				<Provider store={store}>
					<Router onChange={this.handleRoute}>
						<Grid path='/' page='features' />
						<Grid path='/:page' />
					</Router>
				</Provider>
			</div>
		);
	}
}

