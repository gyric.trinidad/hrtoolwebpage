
self.onmessage = ev => {
  
  fetch(ev.data.url)
    .then(res => res.json())
    .then(res => {
      self.postMessage({
        result: res
      });
    })
    .catch(err => {
      // eslint-disable-next-line
      console.log(`Worker >> xhrWorker ${ev.data.url} , ${err.message}`);

      self.postMessage({
        error: err.message,
        status: err.response ? err.response.status : null
      });
    });
};
    
