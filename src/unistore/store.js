import createStore from 'unistore';
import { initialState } from './initialStore';

const store = createStore(initialState);

export default store;