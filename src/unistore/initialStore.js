export const initialState = { 
   form: {
    fetching: false,
    result: false,
    data: null
  },
  price: {
    fetching: false,
    result: false,
    data: null
  },
  features: {
    fetching: false,
    result: false,
    data: null
  },
  devices: {
    fetching: false,
    result: false,
    data: null
  }
};