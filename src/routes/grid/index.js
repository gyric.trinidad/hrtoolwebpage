import style from './style';
import { h, render, Component } from 'preact';
import { connect } from 'unistore/preact';
import { actions } from '_unistore';
import { fetchPageData } from '_mutations';
import * as GridComponents from '_components';

class Grid extends Component {
	
	state = {
		fetching: true,
		result: false,
		data: null
	}

	componentDidMount = () => {
		// fetch data
		fetchPageData(this.props.page)
		// this.props.fetchPageData(this.props.page);
	};

	componentDidUpdate = () => {
		if(this.state.data === null && this.props[this.props.page] !== null) {
			this.setState(this.props[this.props.page])
		};
	}

	renderContent = (data) => {
    try {
      if (data !== undefined && data.component !== undefined) {
        const Compo = GridComponents[data.component] || null;
        if (Compo !== undefined) {
          let props = Object.assign({}, data.props);
          return <Compo {...props} />;
        }
      }
      return null;
    } catch (err) {
      console.error('Widget Component >> renderContent >> Error:', err);
      return null;
    }
  };

	generateStyles = (parentSelector, styles) => {
		try {
			// construct CSS rules from config
			let css = '';
			styles.map((item, index) => {
				css = css + `${parentSelector} ${item.selector}{${item.styles}}\n`;
			});
			return css;
		} catch (err) {
			console.error('Page Styling >> error:', err);
		}
	};
	// data = index, to show
	renderWidgets = (page, data) => {
		return (
			<div id='page-widgets'>
				{data.contents.map((item, i) => {
					return (
						<div
							key={i}
							className={
								item.horizontal ? `scroll-x widget widget${i + 1}` : `widget widget${i + 1}`
							}>
							{this.renderContent(item)}

							<style type='text/css'>
								{this.generateStyles(
									`#page-${page.toLowerCase()} .widget${i + 1}`,
									item.styles ? item.styles : []
								)}
							</style>
						</div>
					);
				})}
				{/* Global Styles */}
				<style type='text/css'>
					{this.generateStyles(
						`#page-${page.toLowerCase()}`,
						data.styles ? data.styles : []
					)}
				</style>
			</div>
		);
	};
	
  render = () => {
		if(this.state.data === null) {
			return (
				<p>No Config found for {this.props.page}.</p>
			)
		}

		return (
			<div class={style.grid} id={`page-${this.props.page}`}>
				{this.renderWidgets(this.props.page, this.state.data)}
			</div>
		);
  };
}
								// json file name
const ConnectComponent = connect(['form','devices','price','features'], actions)(Grid);
export default ConnectComponent;
