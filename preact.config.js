const path = require('path');

// these props are both optional
export default {
	// you can add preact-cli plugins here
	plugins: [],
	/**
	 * Function that mutates the original webpack config.
	 * Supports asynchronous changes when a promise is returned (or it's an async function).
	 *
	 * @param {object} config - original webpack config.
	 * @param {object} env - options passed to the CLI.
	 * @param {WebpackConfigHelpers} helpers - object with useful helpers for working with the webpack config.
	 * @param {object} options - this is mainly relevant for plugins (will always be empty in the config), default to an empty object
	 **/
	webpack(config, env, helpers, options) {
		/** you can change the config here **/

		// add alias
		config.resolve.alias = Object.assign({},
			{
				_unistore: path.resolve(__dirname, "src/unistore"),
				_routes: path.resolve(__dirname, "src/routes"),
				_components: path.resolve(__dirname, "src/components"),
				_mutations: path.resolve(__dirname, "src/mutations"),
				_helpers: path.resolve(__dirname, "src/helpers"),
			},
			config.resolve.alias
		);

		// worker loader
		config.module.rules.push({
			test: path.join(__dirname, 'src/workers'),
			use: { 
				loader: 'worker-loader',
				options: {
					fallback: true
				}
			}
		});

	},
};